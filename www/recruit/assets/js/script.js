function inview(className, root_margin, callback) {
  var nodelist = document.querySelectorAll(className);
  var node = Array.prototype.slice.call(nodelist, 0).reverse();
  var options = {
    root: null,
    rootMargin: root_margin,
    threshold: 0
  }
  var observer = new IntersectionObserver(callback, options);
  node.forEach(function (obj) {
    observer.observe(obj);
  })
}



//init pos
function init_pos(el) {
  var inner = el.find('.js--mask--i');
  var outer = el.find('.js--mask--o');
  var x_set = 101;
  if (el.hasClass('js--mask--r') || el.hasClass('js--child--r')) {
    x_set = x_set * -1;
  }
  gsap.set(inner, {
    x: x_set + '%',
  })
  gsap.set(outer, {
    x: (x_set * -1) + '%',
  })
}

//init TL
function init_tl(el, speed) {
  var inner = el.find('.js--mask--i');
  var outer = el.find('.js--mask--o');
  var delay = 0;
  if (el.data('delay') !== undefined){
    delay = el.data('delay');
  }

  // speed = speed * outer.width()/800;

  var tl = gsap.timeline().pause();
  tl.to([inner, outer], speed, {
    x: 0,
    y: 0,
    delay: delay,
    ease: 'power1.inOut',
    onComplete: function(){
      el.addClass('js--mask--open');
    }
  })
  el.data('tl_mask', tl);
}

function init_tl_order(el, speed) {
  var delay_parent = 0;
  var delay_child = speed / 3;
  if (el.data('delay') !== undefined) {
    delay_parent = el.data('delay');
  }
  if (el.data('child') !== undefined) {
    delay_child = el.data('child');
  }

  var tl = gsap.timeline().delay(delay_parent).pause();

  var time = 0;
  el.find('.js--child--l, .js--child--r').each(function(i){
    time = i * delay_child;
    init_tl($(this), speed);
    tl.add($(this).data('tl_mask').play(), time);
  })

  el.data('tl_mask', tl);
}


/* mask init */
function init_mask(speed){
  var mask_speed = speed;
  var targetClass = '.js--mask--l, .js--mask--r';

  $(targetClass).each(function(){
    var $inner = $(this).wrapInner('<div class="js--mask--i">');
    var $outer = $inner.wrapInner('<div class="js--mask--o">');
    init_pos($(this));
    init_tl($(this), mask_speed);
  })
}

/* mask init child-order */
function init_mask_order(speed){
  var mask_speed = speed;
  var targetClass = '.js--mask--order';

  $(targetClass).find('.js--child--l, .js--child--r').each(function(){
    var $inner = $(this).wrapInner('<div class="js--mask--i">');
    var $outer = $inner.wrapInner('<div class="js--mask--o">');
    init_pos($(this));
  })

  $(targetClass).each(function(){
    init_tl_order($(this), mask_speed);
  })

}


/* mask inview */
function inview_mask(){
  var target = '.js--mask--l, .js--mask--r, .js--mask--order';

  inview(target, '-30% 0px', function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        var tl = $(entry.target).data('tl_mask');
        if (tl !== undefined){
          tl.play();
        }
      }
    });
  });

}


//最小値〜最大値の乱数を返す
function randomIntFromInterval(min, max) {
  return Math.random() * (max - min + 1) + min;
}


function init_glitch_el(el, n){
  var base = el.html();
  el.wrapInner('<span class="base">');
  for(i=0; i<n; i++){
    el.append('<span class="glitch">' + base + '</span>');
  }
}


function slip_pos(el, n){
  //100%をランダムにn分割
  // var splitNum = Math.round(100 / n);
  // var adj = Math.round(splitNum * 0.85);
  var splitNum = 100 / n;
  var adj = splitNum * 0.85;
  var set = 0;
  var ary = [];
  for (i = 0; i < n; i++) {
    var r = Math.round(randomIntFromInterval(adj * -1, adj));
    if (i == n - 1) {
      ary.push([set, 100]);
    } else {
      ary.push([set, splitNum * (i + 1) + r]);
      set = splitNum * (i + 1) + r;
    }
  }

  //ランダムn分割にclip
  el.find('.glitch').each(function (i) {
    var y1 = ary[i][0] + '%';
    var y2 = ary[i][1] + '%';
    gsap.set($(this), {
      clipPath: 'polygon(0 ' + y1 + ', 110% ' + y1 + ', 110% ' + y2 + ', 0 ' + y2 + ')',
    })
  })
}


function init_glitch_pos(el, n){
  slip_pos(el, n);

  //ラインノイズ
  if (el.data('glitch-noise') != undefined){
    el.append('<i class="line"></i>');
  }
}

function glitch_move(el){
  var n = el.find('.glitch').length;
  slip_pos(el, n);
  
  var fontsize = parseInt(el.css('font-size'));
  var slip = 3 + fontsize*0.02;
  //ランダムn分割にclip
  el.find('.glitch').each(function (i) {
    var x = Math.floor(randomIntFromInterval(slip * -1, slip));
    var cx1 = Math.floor(randomIntFromInterval(-3, 3)) + 'px';
    var cx2 = Math.floor(randomIntFromInterval(-6, 6)) + 'px';
    var cx3 = Math.floor(randomIntFromInterval(-8, 8)) + 'px';
    if (el.data('glitchcolor') != undefined){
      gsap.set($(this), {
        width: el.find('.base').width()+1,
        x: x,
        textShadow: cx1 + ' 0 red, ' + cx2 + ' 0 blue, ' + cx3 + ' 0 green',
      })
    }else{
      gsap.set($(this), {
        width: el.find('.base').width()+1,
        x: x,
      })
    }
  })

}


function init_glitch(){

  var targetClass = '.js--glitch, .js--glitch--hover, .js--glitch--child';

  var split_min_base = 3;
  var split_max_base = 5;
  var duration_base = 0.5;
  var repeat_base = 1.25;

  $(targetClass).each(function(){
    
    var $el = $(this);
    $el.addClass('glitch--end');

    if ($el.data('glitch-duration') != undefined) {
      repeat_base = Number($el.data('glitch-duration'));
    }
    
    if ($el.data('glitch') !== undefined) {
      var split_min = split_min_base * $el.data('glitch');
      var split_max = split_max_base * $el.data('glitch');
    }else{
      var split_min = split_min_base;
      var split_max = split_max_base;
    }

    var n = Math.floor(randomIntFromInterval(split_min, split_max));
    init_glitch_el($el, n);
    init_glitch_pos($el, n);

    var tl = gsap.timeline().pause();
    var counter = { var: 0 }; //el指定する用で実質不使用のもの
    if ($el.data('glitchtime') !== undefined){
      var duration = $el.data('glitchtime');
    }else{
      var duration = duration_base;
    }

    tl
    .call(function () { $el.removeClass('glitch--end'); })
    .to(counter, duration, {
      onUpdate: function () {
        counter.var++;
        // console.log(counter.var);
        glitch_move($el);
      },
      onComplete: function () {
        $el.addClass('glitch--end');
        var call = gsap.delayedCall(Math.random() * repeat_base + (repeat_base/10), tl.restart, null, tl);
        $el.data('delayedCall', call);
        // $el.find('.glitch').remove();
        // $el.find('.base').contents().unwrap();
      }
    })
    $el.data('tl_glitch', tl);
  })
  
}


function hover_glitch(){

  //hover
  $('.js--glitch--hover').on('mouseenter', function(){
    $(this).data('tl_glitch').play(0);
  })

  $('.js--glitch--hover').on('mouseleave', function(){
    $(this).data('tl_glitch').progress(1).pause();
    $(this).data('delayedCall').kill();
  })

  //hover glitchは子要素
  $('.js--glitch--hoverwrap').on('mouseenter', function () {
    $(this).find('.js--glitch--child').each(function(){
      $(this).data('tl_glitch').play(0);
    })
  })

  $('.js--glitch--hoverwrap').on('mouseleave', function () {
    $(this).find('.js--glitch--child').each(function () {
      $(this).data('tl_glitch').progress(1).pause();
      $(this).data('delayedCall').kill();
    })
  })

}


function inview_glitch(){
  var target = '.js--glitch';

  inview(target, '-25% 0px', function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        var tl = $(entry.target).data('tl_glitch');
        if (tl !== undefined) {
          tl.play();
          // if ($(entry.target).data('glitch-duration') != undefined){
          //   var duration = Number($(entry.target).data('glitch-duration') * 1000);
          //   console.log(duration);
          //   setTimeout(function(){
          //     tl.progress(1).pause();
          //     // $(entry.target).data('delayedCall').kill();
          //     // $(entry.target).find('.glitch').remove();
          //     // $(entry.target).find('.base').contents().unwrap();
          //   }, duration);
          // }
        }
      }
    });
  });
}
function query_url() {
  //各記事へのリンク（クエリで渡す）
  // ex) href="/recruit/info/?info=id20191114"
  var getUrlQueries = function () {
    var queryStr = window.location.search.slice(1);  // 文頭?を除外
    queries = {};
    // クエリがない場合は空のオブジェクトを返す
    if (!queryStr) {
      return queries;
    }
    // クエリ文字列を & で分割して処理
    queryStr.split('&').forEach(function (queryStr) {
      // = で分割してkey,valueをオブジェクトに格納
      var queryArr = queryStr.split('=');
      queries[queryArr[0]] = queryArr[1];
    });
    return queries;
  }
  var query = getUrlQueries().info;
  if (query) {
    var target = $('#' + query);
    var position = target.offset().top;
    var adj = 0
    if ($('.pageheader')[0]) {
      adj = $('.pageheader').height() + 20;
    }
    $('body,html').animate({ scrollTop: position - adj }, 400, 'swing');
  }
}
function accordion(){

  $('.js--accordion--btn').on('click', function(){
    var $btn = $(this);
    var $warp = $(this).closest('.js--accordion--wrap');
    var $target = $warp.find('.js--accordion');
    if (!$(this).hasClass('accordion--open')){
      //open
      $btn.addClass('accordion--open');
      $target.slideDown();
    }else{
      //close
      $target.slideUp(function(){
        $btn.removeClass('accordion--open');
      });
    }
  })
  
}



function accordion_filter() {
  
  var speed = 0.4;

  $('.js--accordion--filter--wrap .js--accordion--btn').on('click', function(){
    var $btn = $(this);
    var $warp = $(this).closest('.js--accordion--filter--wrap');
    var $target = $warp.find('.js--accordion--filter');
    if (!$(this).hasClass('accordion--open')) {
      //open
      $btn.addClass('accordion--open');
      $target.data('save_h', $target.height()); //展開前の高さを記憶
      gsap.to($target, speed, {
        height: 'auto',
        ease: 'power2.out'
      })
    } else {
      //close
      gsap.to($target, speed*0.5, {
        height: $target.data('save_h'),
        ease: 'none',
        onComplete: function(){
          $btn.removeClass('accordion--open');
        }
      })
      $('body,html').animate({ scrollTop: $warp.closest('.interview__filter').offset().top - $('.pageheader').height() }, speed * 0.5*1000, 'linear');
    }
  })

  var lastInnerWidth = window.innerWidth;
  var timer = false;
  $(window).on('resize', function(){
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function () {
      if (lastInnerWidth != window.innerWidth){
        $('.js--accordion--filter').removeAttr('style');
        $('.js--accordion--filter--wrap').find('.accordion--open').removeClass('accordion--open');
        lastInnerWidth = window.innerWidth;
      }
    }, 200);
  })



}
function modal(speed){

  function open_modal(speed){
    $('body').addClass('modal--open');
    gsap.set('.js--modal', {
      display: 'flex',
    })
    gsap.to('.js--modal', speed, {
      opacity: 1,
    })
  }

  function close_modal(speed){
    gsap.to('.js--modal', speed, {
      opacity: 0,
      onComplete: function(){
        $('.js--modal').find('[data-modal]').hide();
        gsap.set('.js--modal', { display: 'none' });
        $('body').removeClass('modal--open');
      }
    })
  }

  $('.js--modal--btn').on('click', function(){
    var key = $(this).data('modal');
    var $target = $('.js--modal').find('[data-modal="' + key + '"]');
    $target.show();
    open_modal(speed);
  })

  $('.js--modal--close').on('click', function(){
    close_modal(speed);
  })
  $('.js--modal').on('click', function (event) {
    if (!$(event.target).closest('.modal__window').length) {
      close_modal(speed);
    }
  })


}
/* about - entm */
function carousel_entm(){

  function check_category_navi(el, navi){
    var category = el.find('.swiper-slide-active').data('category');
    var current = navi.find('[data-category="' + category + '"]');
    current.siblings().removeClass('active');
    current.addClass('active');
  }

  function control_navi(el, navi){
    navi.find('[data-category]').on('click', function(){
      var category = $(this).data('category');
      var target = el.find('.swiper-slide[data-category="' + category + '"]')[0];
      var index = $(target).data('swiper-slide-index');
      el.data('swiper').slideTo(index + 1);
    })
  }

  function init_carousel(el, navi){
    var swiper = new Swiper(el, {
      speed: 750,
      spaceBetween: 0,
      slidesPerView: 1,
      loop: true,
      navigation: {
        nextEl: el.find('.swiper-button-next'),
        prevEl: el.find('.swiper-button-prev'),
      },
      on: {
        init: function (){
          check_category_navi(el, navi);
        },
        slideChangeTransitionStart: function(){
          check_category_navi(el, navi);
        }
      },
    });
    el.data('swiper', swiper);
  }

  $('.js--carousel--entm').each(function(){
    var navi = $(this).closest('.js--carousel--wrap').find('.js--carousel--navi');
    init_carousel($(this), navi);
    control_navi($(this), navi);
  })

}


/* about - program */
function carousel_program(){
  var swiper_program = [];

  function init_carousel() {
    $('.js--carousel--program').each(function () {
      var swiper = new Swiper($(this), {
        speed: 750,
        spaceBetween: 0,
        slidesPerView: 1,
        loop: true,
        navigation: {
          nextEl: $(this).find('.swiper-button-next'),
          prevEl: $(this).find('.swiper-button-prev'),
        },
        pagination: {
          el: $(this).find('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
        },
      });
      swiper_program.push(swiper);
    })
  }

  var breakpoint = window.matchMedia('(min-width:769px)');
  var breakpointChecker = function () {
    if (breakpoint.matches === true) {
      if (swiper_program.length > 0) {
        for (i = 0; i < swiper_program.length; i++) {
          if (swiper_program[i] != undefined && swiper_program[i].initialized) {
            swiper_program[i].destroy();
          }
        }
        swiper_program = [];
      }
    } else if (breakpoint.matches === false) {
      init_carousel();
    }
  }

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();

}


/* workstyle */
function carousel_workstyle(){
  var swiper_workstyle = [];

  function init_carousel() {
    $('.js--carousel--workstyle').each(function () {
      var swiper = new Swiper($(this), {
        speed: 750,
        spaceBetween: 0,
        slidesPerView: 1,
        loop: true,
        navigation: {
          nextEl: $(this).find('.swiper-button-next'),
          prevEl: $(this).find('.swiper-button-prev'),
        },
        pagination: {
          el: $(this).find('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
        },
      });
      swiper_workstyle.push(swiper);
    })
  }

  var breakpoint = window.matchMedia('(min-width:769px)');
  var breakpointChecker = function () {
    if (breakpoint.matches === true) {
      if (swiper_workstyle.length > 0) {
        for (i = 0; i < swiper_workstyle.length; i++) {
          if (swiper_workstyle[i] != undefined && swiper_workstyle[i].initialized) {
            swiper_workstyle[i].destroy();
          }
        }
        swiper_workstyle = [];
      }
    } else if (breakpoint.matches === false) {
      init_carousel();
    }
  }

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();

}


/* technology */
function carousel_flow(){
  function init_carousel(el) {
    var swiper = new Swiper(el, {
      speed: 750,
      spaceBetween: 0,
      slidesPerView: 1,
      loop: true,
      navigation: {
        nextEl: el.find('.swiper-button-next'),
        prevEl: el.find('.swiper-button-prev'),
      },
      pagination: {
        el: el.find('.swiper-pagination'),
        type: 'bullets',
        clickable: true,
      },
    });
    el.data('swiper', swiper);
  }
  
  $('.js--carousel--flow').each(function () {
    init_carousel($(this));
  })
}


/* training */
function carousel_rotation() {
  var swiper_rotation = [];

  function init_carousel() {
    $('.js--carousel--rotation').each(function () {
      var swiper = new Swiper($(this), {
        speed: 750,
        spaceBetween: 0,
        slidesPerView: 1,
        loop: true,
        navigation: {
          nextEl: $(this).find('.swiper-button-next'),
          prevEl: $(this).find('.swiper-button-prev'),
        },
        pagination: {
          el: $(this).find('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
        },
      });
      swiper_rotation.push(swiper);
    })
  }

  var breakpoint = window.matchMedia('(min-width:769px)');
  var breakpointChecker = function () {
    if (breakpoint.matches === true) {
      if (swiper_rotation.length > 0) {
        for (i = 0; i < swiper_rotation.length; i++) {
          if (swiper_rotation[i] != undefined && swiper_rotation[i].initialized) {
            swiper_rotation[i].destroy();
          }
        }
        swiper_rotation = [];
      }
    } else if (breakpoint.matches === false) {
      init_carousel();
    }
  }

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();

}


function carousel_support() {
  var swiper_support = [];

  function init_carousel() {
    $('.js--carousel--support').each(function () {
      var swiper = new Swiper($(this), {
        speed: 750,
        spaceBetween: 0,
        slidesPerView: 1,
        loop: true,
        navigation: {
          nextEl: $(this).find('.swiper-button-next'),
          prevEl: $(this).find('.swiper-button-prev'),
        },
        pagination: {
          el: $(this).find('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
        },
      });
      swiper_support.push(swiper);
    })
  }

  var breakpoint = window.matchMedia('(min-width:769px)');
  var breakpointChecker = function () {
    if (breakpoint.matches === true) {
      if (swiper_support.length > 0) {
        for (i = 0; i < swiper_support.length; i++) {
          if (swiper_support[i] != undefined && swiper_support[i].initialized) {
            swiper_support[i].destroy();
          }
        }
        swiper_support = [];
      }
    } else if (breakpoint.matches === false) {
      init_carousel();
    }
  }

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();

}

function inview_interview(){

  $('.js--inview--interview').find('.item').each(function(){
    gsap.set($(this), { opacity: 0 });
    $(this).addClass('showing');
  })


  inview('.js--inview--interview', '-35% 0px', function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        $('.js--inview--interview').find('.showing').each(function (i) {
          var $el = $(this);
          gsap.set($el, {
            delay: i * 0,
            opacity: 1,
            onComplete: function () {
              setTimeout(function () {
                $el.removeClass('showing');
              }, 200);
            }
          })
        })
        observer.unobserve(entry.target);
      }
    });
  });
}



function filter_attr(){

  function switching_item(el, anchor_speed){
    //チェックボックスON/OFF
    el.siblings().removeClass('on');
    el.addClass('on');

    //アンカー移動
    var position = $('#filter_anchor').offset().top - $('.pageheader').height();
    $('body,html').animate({ scrollTop: position }, anchor_speed, 'swing', function(){
      var tl = $('#filter_anchor').data('tl_mask');
      if (tl !== undefined) {
        tl.play();
      }
    });

    //リスト切り替え
    var key = "'" + el.data('key') + "'";
    if (key == "'ALL'") {
      $('.js--filter--target').find('[data-key]').show().addClass('showing');
    } else {
      $('.js--filter--target').find('[data-key]').each(function () {
        gsap.set($(this), { opacity: 0 })
        var taeget_key = $(this).data('key');
        if (taeget_key.indexOf(key) != -1) {
          $(this).show().addClass('showing');
        } else {
          $(this).hide().removeClass('showing');
        }
      })
    }

    $('.js--filter--target').find('.showing').each(function (i) {
      var $el = $(this);
      gsap.set($el, {
        delay: i * 0.075,
        opacity: 1,
        onComplete: function () {
          setTimeout(function () {
            $el.removeClass('showing');
          }, 200);
        }
      })
    })
  }


  /* フィルターリストによる切り替え */
  $('.js--filter .attr').on('click', function(){
    switching_item($(this), 300);
  })

  
  /* クエリ付きURLによる切り替え */
  var query = decodeURI(window.location.search);
  if (query != '' ){
    var keyStr = query.split('=')[1];
    var $targetCheck = $('.js--filter').find('[data-key="' + keyStr + '"]');

    var timer;
    //3秒後にpageがinitしていなかったら強制init
    timer = setTimeout(function () {
      switching_item($targetCheck, 0);
    }, 3000);
    var observer = new MutationObserver(function (records) {
      var html_class = document.documentElement.className;
      if (html_class.indexOf('wf-active') != -1) { //adobeフォント適用後実行
        if (timer > 0) {
          clearTimeout(timer);
        }
        switching_item($targetCheck, 0);
        observer.disconnect();
      }
    })
    observer.observe(document.documentElement, {
      attributes: true
    })
  }


}
function open_menu(speed){
  $('body').addClass('menu--open');
  gsap.set('.js--menu', {
    display: 'block',
  })
  gsap.set('.js--menu', {
    opacity: 1,
    onComplete: function(){
      $('.js--menu--btn').removeAttr('style');
    }
  })
}

function close_menu(speed){
  $('body').removeClass('menu--open');
  gsap.set('.js--menu', {
    opacity: 0,
    onComplete: function(){
      $('.js--menu--btn').removeAttr('style');
      gsap.set($('.js--menu'), {
        display: 'none',
      })
    }
  })
}

function check_menu_h(){
  var inner_h = $('.js--menu .menu__inner').height();
  var win_h = $(window).height();
  var cnv_h = $('.js--menu .navi--cnv').height();
  if (inner_h > win_h - cnv_h){
    $('.js--menu').addClass('cnv--fixed');
  }else{
    $('.js--menu').removeClass('cnv--fixed');
  }
}

function menu_mask(speed){

  function init_pos(el){
    var inner = el.find('.js--mask--i');
    var outer = el.find('.js--mask--o');
    gsap.set(inner, {
      x: '100%',
    })
    gsap.set(outer, {
      x: '-100%',
    })
  }

  function init_tl_sub(el){
    var inner = el.find('.js--mask--i');
    var outer = el.find('.js--mask--o');
    var tl_open = gsap.timeline();
    tl_open
    .set(inner, {x: '-100%'})
    .set(outer, {x: '100%'})
    .set([outer, inner], { opacity: 1 })
    .to([inner, outer], speed, {
      x: 0,
      ease: 'power2.out',
    })
    el.data('tl_mask_open', tl_open);
    
    var tl_close = gsap.timeline();
    tl_close
    .to(inner, speed, {
      x: '100%',
      ease: 'power2.out',
    }, 0)
    .to(outer, speed, {
      x: '-100%',
      ease: 'power2.out',
      onComplete: function(){
        gsap.set([outer, inner], { opacity: 0 })
      }
    }, 0)
    el.data('tl_mask_close', tl_close);
  }


  $('.js--menu--mask').each(function(){
    var $inner = $(this).wrapInner('<div class="js--mask--i">');
    var $outer = $inner.wrapInner('<div class="js--mask--o">');
    init_pos($(this));
    init_tl_sub($(this));
  })

  //tl - open
  var tl_open = gsap.timeline().pause();
  var time = 0;
  $('.js--menu--mask').each(function (i) {
    time = i * 0.06;
    tl_open.add($(this).data('tl_mask_open'), time);
  })
  $('.js--menu').data('tl_mask_open', tl_open);

  //tl - close
  var tl_close = gsap.timeline().pause();
  var time = 0;
  $('.js--menu--mask').each(function (i) {
    time = i * 0.06;
    tl_close.add($(this).data('tl_mask_close'), time);
  })
  tl_close.call(function(){
    close_menu(0.2);
  }, null, time + speed)
  $('.js--menu').data('tl_mask_close', tl_close);

}



function menu(){
  var speed = 0.35;

  $('.js--menu--btn').on('click', function(){
    $(this).css('pointer-events', 'none');
    if (!$('body').hasClass('menu--open')){
      open_menu(speed);
      // check_menu_h();
      //mask
      if($('.js--menu').data('tl_mask_open') !== undefined){
        $('.js--menu').data('tl_mask_open').play(0);
      }
    }else{
      //mask
      if ($('.js--menu').data('tl_mask_close') !== undefined) {
        $('.js--menu').data('tl_mask_close').play(0);
      }else{
        close_menu(speed);
      }
    }
  })

  $('.js--menu').on('click', function (event){
    if (!$(event.target).closest('.menu__inner').length) {
      //mask
      if ($('.js--menu').data('tl_mask_close') !== undefined) {
        $('.js--menu').data('tl_mask_close').play(0);
      } else {
        close_menu(speed);
      }
    }
  })

  /*
  var timer = false;
  $(window).on('resize', function(){
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function () {
      check_menu_h();
    }, 300);
  })
  */


}
function noise_bg(){
  var noise_el = '<div class="noise"></div>';
  var noise_line_el = '<div class="noise_line"></div>';
  $('.js--bg--noise').append(noise_el);
  $('.js--bg--noise').append(noise_line_el);

  var duration = 0.4;
  var tl = gsap.timeline();

  tl.to(['.js--bg--noise .noise', '.js--bg--noise .noise_line'], duration, {
    backgroundPosition: "0px 25%",
    ease: 'none',
    onStart: function () {
      $('.js--bg--noise').addClass('noise_on');
    },
    onComplete: function () {
      $('.js--bg--noise').removeClass('noise_on');
    }
  })

  function loop() {
    var rand = Math.round(Math.random() * (8000 - 4000)) + 4000;
    var random_duration = Math.random() + 0.25;
    setTimeout(function () {
      tl.timeScale(random_duration).play(0);
      loop();
    }, rand);
  };

  loop();

}

function split_phChange(speed){
  /* sticky polyfill */
  var elements = $('.split__ph');
  Stickyfill.add(elements);

  function ph_on(el, speed){
    var trigger = el.data('trigger');
    var ph_container = el.closest('.js--split').find('.js--split--ph');
    var targetPh = ph_container.find('[data-trigger="' + trigger + '"]');
    el.addClass('on');
    targetPh.addClass('on');
    // $('.js--split--ph').addClass('on');
    ph_container.addClass('on');
    
    var tl = targetPh.data('tl_mask');
    targetPh.removeClass('js--mask--open');
    if (tl !== undefined) {
      tl.play(0);
    }
  }

  function ph_off_other(el, speed){
    var trigger = el.data('trigger');
    var ph_container = el.closest('.js--split').find('.js--split--ph');
    var targetPh = ph_container.find('[data-trigger="' + trigger + '"]');

    //text
    var other = el.siblings('.js--split--trigger');
    var other_ph = targetPh.siblings();
    other.removeClass('on');
    other_ph.removeClass('on');

    other_ph.each(function(){
      var tl = $(this).data('tl_mask');
      if (tl !== undefined) {
        tl.pause(0);
      }
    })
  }

  var observer_ary = [];
  var pos = {};

  //init
  function init(){
    if ($(window).width() > 768){
      //pc
      pos = {
        start_el: '0',
        start_window: '50%',
        end_el: '100%',
        end_window: '50%',
      }
    } else if ($(window).width() > 450 && $(window).width() <= 768){
      //mid
      pos = {
        start_el: '-75px',
        start_window: '485px',
        end_el: '100%',
        end_window: '485px',
      }
    } else {
      //sp
      pos = {
        start_el: '-70px',
        start_window: '275px',
        end_el: '100%',
        end_window: '275px',
      }
    }
    
    $('.js--split--trigger').each(function(){
      var $el = $(this);
      var observer = ScrollTrigger.create({
        trigger: $el,
        start: pos.start_el + ' ' + pos.start_window,
        end: pos.end_el + ' ' + pos.end_window,
        // markers: true,
        onEnter: function () {
          ph_on($el, speed);
          ph_off_other($el, speed);
        },
        onEnterBack: function () {
          ph_on($el, speed);
          ph_off_other($el, speed);
        },
        onLeave: function(){
          $el.removeClass('on');
          $el.closest('.js--split').find('.js--split--ph').removeClass('on');
        },
        onLeaveBack: function(){
          $el.removeClass('on');
          $el.closest('.js--split').find('.js--split--ph').removeClass('on');
        }
      });
  
      observer_ary.push(observer);
    })
  }

  init();

  var timer = false;
  $(window).on('resize', function(){
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function () {
      for (i = 0; i < observer_ary.length; i++){
        observer_ary[i].kill(true);
      }
      observer_ary = [];
      init();
    }, 300);
  })

  


  if ($('.js--mask--ph')[0]){
    var mask_speed = 0.2;
  
    $('.js--mask--ph').each(function () {
      var $inner = $(this).wrapInner('<div class="js--mask--i">');
      var $outer = $inner.wrapInner('<div class="js--mask--o">');
      init_pos($(this));
      init_tl($(this), mask_speed);
    })
  }


  $('.js--split').each(function () {
    var $first = $(this).find('.js--split--trigger:first');
    ph_on($first, speed);
    $('.js--split--ph').addClass('on');
  })
  
}


/* menu */
if ($('.js--menu')[0]){
  menu();
  if($('.js--menu--mask')[0]){
    menu_mask(0.2); //arg: マスクスピード
  }
}

/* modal */
if ($('.js--modal--btn')[0]){
  modal(0.25); //arg: フェードインスピード
}

/* mask */
if ($('.js--mask--l, .js--mask--r')[0]){
  init_mask(0.3); //arg: マスクスピード
  inview_mask();
}
if ($('.js--mask--order')[0]){
  init_mask_order(0.3); //arg: マスクスピード
  inview_mask();
}

/* glitch */
if ($('.js--glitch, .js--glitch--hover, .js--glitch--child')[0]){
  init_glitch();
}
if ($('.js--glitch--hover, .js--glitch--hoverwrap')[0]){
  hover_glitch();
}
if ($('.js--glitch')[0]){
  inview_glitch();
}


/* ページ内アンカー */
$('a[href^="#"]').on("click", function () {
  var href = $(this).attr("href");
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - $('.pageheader').height();
  if (target.hasClass('js--accordion--wrap')){
    //飛び先がアコーディオンの場合
    $('body,html').animate({ scrollTop: position }, 400, 'swing', function(){
      var $btn = target.find('.js--accordion--btn');
      if (!$btn.hasClass('accordion--open')){
        var $target = target.find('.js--accordion');
        $btn.addClass('accordion--open');
        $target.slideDown();
      }
    });
  }else{
    //通常
    $('body,html').animate({ scrollTop: position }, 400, 'swing');
  }
  return false;
});

/* クエリ付きURLアンカー */
if ($('#page--news')[0]){
  query_url();
}


/* アコーディオン */
if ($('.js--accordion')[0]){
  accordion();
}
if ($('.js--accordion--filter')[0]){
  accordion_filter();
}

/* ページ背景ノイズ */
if ($('.js--bg--noise')[0]){
  noise_bg();
}



/* about */
if ($('.js--carousel--entm')[0]){
  carousel_entm();
}
if ($('.js--carousel--program')[0]){
  carousel_program();
}

/* workstyle */
if ($('.js--carousel--workstyle')[0]){
  carousel_workstyle();
}

/* technology */
if ($('.js--carousel--flow')[0]){
  carousel_flow();
}

/* training */
if ($('.js--carousel--rotation')[0]){
  carousel_rotation();
}
if ($('.js--carousel--support')[0]){
  carousel_support();
}

/* split article */
if ($('.js--split')[0]){
  var timer_init;
  //3秒後にpageがinitしていなかったら強制init
  timer_init = setTimeout(function () {
    split_phChange(1);
  }, 3000);
  var observer = new MutationObserver(function (records) {
    var html_class = document.documentElement.className;
    if (html_class.indexOf('wf-active') != -1) { //adobeフォント適用後実行
      if (timer_init > 0) {
        clearTimeout(timer_init);
      }
      split_phChange(1);
      observer.disconnect();
    }
  })
  observer.observe(document.documentElement, {
    attributes: true
  })
}

/* interview */
if ($('.js--inview--interview')[0]){
  inview_interview();
}
if ($('.js--filter')[0]){
  filter_attr();
}